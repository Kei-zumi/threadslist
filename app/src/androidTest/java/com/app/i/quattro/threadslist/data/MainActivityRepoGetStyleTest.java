package com.app.i.quattro.threadslist.data;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.util.Log;

import com.app.i.quattro.threadslist.realmObj.Genre;
import com.app.i.quattro.threadslist.realmObj.Style;
import com.app.i.quattro.threadslist.test.R;

import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;

import io.realm.Realm;
import io.realm.RealmConfiguration;

import static junit.framework.Assert.assertEquals;

public class MainActivityRepoGetStyleTest {
    Realm realm;
    private Context testContext;
    private static final String[] firstOfEachStyle =
            {"Blouse","Crew","DressShirt","1-Shoulder","Denim","Duffel","Blouson","Boots","Chino","A-line","Booties","Ascot","Belt","Ring"};

    @Before
    public void setup() throws Exception {
        testContext = InstrumentationRegistry.getInstrumentation().getContext();

        Realm.init(InstrumentationRegistry.getTargetContext());
        RealmConfiguration testConfig = new RealmConfiguration.Builder()
                .schemaVersion(0)
                .inMemory()
                .build();
        Realm.setDefaultConfiguration(testConfig);
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new JsonImportTransaction());
    }

    @Test
    public void getStyleLowerOutOfRangeId() throws Exception{
        String genreId = "0";
        int season = 0;
        Boolean womens = true;

        MainActivityRepository mainActivityRepo = new MainActivityRepository();
        String expected = "Style";
        String[] testvalue = mainActivityRepo.getStyle(genreId, season, womens);
        assertEquals(expected, testvalue[0]);
    }
    @Test
    public void getStyleUpperOutOfRangeId() throws Exception{
        String genreId = "15";
        int season = 0;
        Boolean womens = true;

        MainActivityRepository mainActivityRepo = new MainActivityRepository();
        String[] testvalue = mainActivityRepo.getStyle(genreId, season, womens);
        String expected = "Style";
        assertEquals(expected, testvalue[0]);
    }
    @Test
    public void getStyleTops() throws Exception{
        String genreId = "1";
        int season = 0;
        Boolean womens = true;

        MainActivityRepository mainActivityRepo = new MainActivityRepository();
        String[] testvalue = mainActivityRepo.getStyle(genreId, season, womens);
        int array_size = 7;
        assertEquals(array_size, testvalue.length);
        assertEquals(firstOfEachStyle[0], testvalue[1]);
    }
    @Test
    public void getStyleTshirt() throws Exception{
        String genreId = "2";
        int season = 0;
        Boolean womens = false;

        MainActivityRepository mainActivityRepo = new MainActivityRepository();
        String[] testvalue = mainActivityRepo.getStyle(genreId, season, womens);
        int array_size = 9;
        assertEquals(array_size, testvalue.length);
        assertEquals(firstOfEachStyle[1], testvalue[1]);
    }
    @Test
    public void getStyleShirts() throws Exception{
        String genreId = "3";
        int season = 0;
        Boolean womens = false;

        MainActivityRepository mainActivityRepo = new MainActivityRepository();
        String[] testvalue = mainActivityRepo.getStyle(genreId, season, womens);
        int array_size = 8;
        assertEquals(array_size, testvalue.length);
        assertEquals(firstOfEachStyle[2], testvalue[1]);
    }
    @Test
    public void getStyleDress() throws Exception{
        String genreId = "4";
        int season = 0;
        Boolean womens = true;

        MainActivityRepository mainActivityRepo = new MainActivityRepository();
        String[] testvalue = mainActivityRepo.getStyle(genreId, season, womens);
        int array_size = 12;
        assertEquals(array_size, testvalue.length);
        assertEquals(firstOfEachStyle[3], testvalue[1]);
    }
    @Test
    public void getStyleJacket() throws Exception{
        String genreId = "5";
        int season = 1;
        Boolean womens = false;

        MainActivityRepository mainActivityRepo = new MainActivityRepository();
        String[] testvalue = mainActivityRepo.getStyle(genreId, season, womens);
        int array_size = 9;
        assertEquals(array_size, testvalue.length);
        assertEquals(firstOfEachStyle[4], testvalue[1]);
    }
    @Test
    public void getStyleCoat() throws Exception{
        String genreId = "6";
        int season = 1;
        Boolean womens = false;

        MainActivityRepository mainActivityRepo = new MainActivityRepository();
        String[] testvalue = mainActivityRepo.getStyle(genreId, season, womens);
        int array_size = 7;
        assertEquals(array_size, testvalue.length);
        assertEquals(firstOfEachStyle[5], testvalue[1]);
    }
    @Test
    public void getStyleOuter() throws Exception{
        String genreId = "7";
        int season = 0;
        Boolean womens = false;

        MainActivityRepository mainActivityRepo = new MainActivityRepository();
        String[] testvalue = mainActivityRepo.getStyle(genreId, season, womens);
        int array_size = 6;
        assertEquals(array_size, testvalue.length);
        assertEquals(firstOfEachStyle[6], testvalue[1]);
    }
    @Test
    public void getStyleJeans() throws Exception{
        String genreId = "8";
        int season = 0;
        Boolean womens = false;

        MainActivityRepository mainActivityRepo = new MainActivityRepository();
        String[] testvalue = mainActivityRepo.getStyle(genreId, season, womens);
        int array_size = 7;
        assertEquals(array_size, testvalue.length);
        assertEquals(firstOfEachStyle[7], testvalue[1]);
    }
    @Test
    public void getStylePantsMensSs() throws Exception{
        String genreId = "9";
        int season = 0;
        Boolean womens = false;

        MainActivityRepository mainActivityRepo = new MainActivityRepository();
        String[] testvalue = mainActivityRepo.getStyle(genreId, season, womens);
        String expected = "Shorts";
        int array_size = 4;
        assertEquals(array_size, testvalue.length);
        assertEquals(expected, testvalue[2]);
    }
    @Test
    public void getStylePantsMensFw() throws Exception{
        String genreId = "9";
        int season = 1;
        Boolean womens = false;

        MainActivityRepository mainActivityRepo = new MainActivityRepository();
        String[] testvalue = mainActivityRepo.getStyle(genreId, season, womens);
        int array_size = 3;
        assertEquals(array_size, testvalue.length);
        assertEquals(firstOfEachStyle[8], testvalue[1]);
    }
    @Test
    public void getStylePantsWomensSs() throws Exception{
        String genreId = "9";
        int season = 0;
        Boolean womens = true;

        MainActivityRepository mainActivityRepo = new MainActivityRepository();
        String[] testvalue = mainActivityRepo.getStyle(genreId, season, womens);
        String fourth_value = "Shorts";
        int array_size = 7;
        assertEquals(array_size, testvalue.length);
        assertEquals(fourth_value, testvalue[4]);
    }
    @Test
    public void getStylePantsWomensFw() throws Exception{
        String genreId = "9";
        int season = 1;
        Boolean womens = true;

        MainActivityRepository mainActivityRepo = new MainActivityRepository();
        String[] testvalue = mainActivityRepo.getStyle(genreId, season, womens);
        int array_size = 6;
        assertEquals(array_size, testvalue.length);
    }
    @Test
    public void getStyleSkirt() throws Exception{
        String genreId = "10";
        int season = 0;
        Boolean womens = true;

        MainActivityRepository mainActivityRepo = new MainActivityRepository();
        String[] testvalue = mainActivityRepo.getStyle(genreId, season, womens);
        int array_size = 13;
        assertEquals(array_size, testvalue.length);
        assertEquals(firstOfEachStyle[9], testvalue[1]);
    }
    @Test
    public void getStyleFootwearMens() throws Exception{
        String genreId = "11";
        int season = 0;
        Boolean womens = false;

        MainActivityRepository mainActivityRepo = new MainActivityRepository();
        String[] testvalue = mainActivityRepo.getStyle(genreId, season, womens);
        String expected = "Boots";
        int array_size = 9;
        assertEquals(array_size, testvalue.length);
        assertEquals(expected, testvalue[1]);
    }
    @Test
    public void getStyleFootwearWomens() throws Exception{
        String genreId = "11";
        int season = 0;
        Boolean womens = true;

        MainActivityRepository mainActivityRepo = new MainActivityRepository();
        String[] testvalue = mainActivityRepo.getStyle(genreId, season, womens);
        int array_size = 13;
        assertEquals(array_size, testvalue.length);
        assertEquals(firstOfEachStyle[10], testvalue[1]);
    }
    @Test
    public void getStyleHat() throws Exception{
        String genreId = "12";
        int season = 0;
        Boolean womens = false;

        MainActivityRepository mainActivityRepo = new MainActivityRepository();
        String[] testvalue = mainActivityRepo.getStyle(genreId, season, womens);
        int array_size = 11;
        assertEquals(array_size, testvalue.length);
        assertEquals(firstOfEachStyle[11], testvalue[1]);
    }
    @Test
    public void getStyleAccessoriesFw() throws Exception{
        String genreId = "13";
        int season = 1;
        Boolean womens = false;

        MainActivityRepository mainActivityRepo = new MainActivityRepository();
        String[] testvalue = mainActivityRepo.getStyle(genreId, season, womens);
        String third_value = "Glove";
        int array_size = 7;
        assertEquals(array_size, testvalue.length);
        assertEquals(firstOfEachStyle[12], testvalue[1]);
        assertEquals(third_value, testvalue[3]);
    }
    @Test
    public void getStyleAccessoriesSs() throws Exception{
        String genreId = "13";
        int season = 0;
        Boolean womens = false;

        MainActivityRepository mainActivityRepo = new MainActivityRepository();
        String[] testvalue = mainActivityRepo.getStyle(genreId, season, womens);
        String third_value = "Sunglass";
        int array_size = 5;
        assertEquals(array_size, testvalue.length);
        assertEquals(firstOfEachStyle[12], testvalue[1]);
        assertEquals(third_value, testvalue[3]);
    }
    @Test
    public void getStyleJewelryMens() throws Exception{
        String genreId = "14";
        int season = 0;
        Boolean womens = false;

        MainActivityRepository mainActivityRepo = new MainActivityRepository();
        String[] testvalue = mainActivityRepo.getStyle(genreId, season, womens);
        int array_size = 6;
        assertEquals(array_size, testvalue.length);
        assertEquals(firstOfEachStyle[13], testvalue[1]);
    }
    @Test
    public void getStyleJewelryWomens() throws Exception{
        String genreId = "14";
        int season = 0;
        Boolean womens = true;

        MainActivityRepository mainActivityRepo = new MainActivityRepository();
        String[] testvalue = mainActivityRepo.getStyle(genreId, season, womens);
        String expected = "Earrings";
        int array_size = 8;
        assertEquals(array_size, testvalue.length);
        assertEquals(expected, testvalue[3]);
    }

    private class JsonImportTransaction implements Realm.Transaction{
        @Override
        public void execute(Realm realm) {
            try {
                InputStream inputStream = testContext.getResources().openRawResource(R.raw.genre);
                realm.createAllFromJson(Genre.class, inputStream);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                InputStream inputStream = testContext.getResources().openRawResource(R.raw.style);
                realm.createAllFromJson(Style.class, inputStream);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}