package com.app.i.quattro.threadslist.data;

import android.content.Context;
import android.support.test.InstrumentationRegistry;

import com.app.i.quattro.threadslist.test.R;
import com.app.i.quattro.threadslist.realmObj.Genre;
import com.app.i.quattro.threadslist.realmObj.Style;

import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;

import io.realm.Realm;
import io.realm.RealmConfiguration;

import static junit.framework.Assert.assertEquals;

public class MainActivityRepositoryTest {
    Realm realm;
    private Context testContext;

    @Before
    public void setup() throws Exception {
        testContext = InstrumentationRegistry.getInstrumentation().getContext();

        Realm.init(InstrumentationRegistry.getTargetContext());
        RealmConfiguration testConfig = new RealmConfiguration.Builder()
                .schemaVersion(0)
                .inMemory()
                .build();
        Realm.setDefaultConfiguration(testConfig);

        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new JsonImportTransaction());
    }

//  getExtraAttribute() test
    @Test
    public void getExtraAttribute() throws Exception{
        String genreId = "4";

        MainActivityRepository mainActivityRepo = new MainActivityRepository();
        String testvalue = mainActivityRepo.getExtraAttribute(genreId);
        String expected = "skirt_dress_length";
        assertEquals(expected, testvalue);
        String genreId2 = "10";
        String testvalue2 = mainActivityRepo.getExtraAttribute(genreId2);
        assertEquals(expected, testvalue2);
    }
//  null query simply masked the import failure of pre-populated DB, so it only means sth when testGetExtraAttribute() passes
    @Test
    public void getExtraAttributeNull() throws Exception{
        String genreId = "1";

        MainActivityRepository mainActivityRepo = new MainActivityRepository();
        String testvalue = mainActivityRepo.getExtraAttribute(genreId);
        assertEquals(null, testvalue);
    }
    @Test
    public void getExtraAttributeLowerOutOfRangeId() throws Exception{
        String genreId = "0";

        MainActivityRepository mainActivityRepo = new MainActivityRepository();
        String testvalue = mainActivityRepo.getExtraAttribute(genreId);
        assertEquals(null, testvalue);
    }
    @Test
    public void getExtraAttributeUpperOutOfRangeId() throws Exception{
        String genreId = "15";

        MainActivityRepository mainActivityRepo = new MainActivityRepository();
        String testvalue = mainActivityRepo.getExtraAttribute(genreId);
        assertEquals(null, testvalue);
    }

//  GetTabtitles() test
    @Test
    public void getTabTitlesFirstItem() throws Exception{
        int season = 0;
        boolean womans = false;

        MainActivityRepository mainActivityRepo = new MainActivityRepository();
        String[] testvalue = mainActivityRepo.getTabTitles(season, womans);
        String expected = "All item";
        assertEquals(expected, testvalue[0]);
    }
    @Test
    public void getTabTitlesMensSs() throws Exception{
        int season = 0;
        boolean womans = false;

        MainActivityRepository mainActivityRepo = new MainActivityRepository();
        String[] testvalue = mainActivityRepo.getTabTitles(season, womans);
        String expected = "Outer";
        assertEquals(expected, testvalue[3]);
    }
    @Test
    public void getTabTitlesMensFw() throws Exception{
        int season = 1;
        boolean womans = false;

        MainActivityRepository mainActivityRepo = new MainActivityRepository();
        String[] testvalue = mainActivityRepo.getTabTitles(season, womans);
        String expected = "Jacket";
        assertEquals(expected, testvalue[3]);
    }
    @Test
    public void getTabTitlesWomensSs() throws Exception{
        int season = 0;
        boolean womans = true;

        MainActivityRepository mainActivityRepo = new MainActivityRepository();
        String[] testvalue = mainActivityRepo.getTabTitles(season, womans);
        String expected = "Tops";
        assertEquals(expected, testvalue[1]);
    }
    @Test
    public void getTabTitlesWomensFw() throws Exception{
        int season = 1;
        boolean womans = true;

        MainActivityRepository mainActivityRepo = new MainActivityRepository();
        String[] testvalue = mainActivityRepo.getTabTitles(season, womans);
        String expected = "Jacket";
        assertEquals(expected, testvalue[5]);
    }
    @Test
    public void LastTabTitle() throws Exception{
        int season = 1;
        boolean womans = true;

        MainActivityRepository mainActivityRepo = new MainActivityRepository();
        String[] testvalue = mainActivityRepo.getTabTitles(season, womans);
        String expected = "Jewelry";
        assertEquals(expected, testvalue[14]);
    }

//  GetGenreIds() test
    @Test
    public void getGenreIdsFirstItem() throws Exception{
        int season = 0;
        boolean womans = false;

        MainActivityRepository mainActivityRepo = new MainActivityRepository();
        String[] testvalue = mainActivityRepo.getGenreIds(season, womans);
        String expected = "All item";
        assertEquals(expected, testvalue[0]);
    }
    @Test
    public void getGenreIdsMensSs() throws Exception{
        int season = 0;
        boolean womans = false;

        MainActivityRepository mainActivityRepo = new MainActivityRepository();
        String[] testvalue = mainActivityRepo.getGenreIds(season, womans);
        String expected = "7";
        assertEquals(expected, testvalue[3]);
    }
    @Test
    public void getGenreIdsMensFw() throws Exception{
        int season = 1;
        boolean womans = false;

        MainActivityRepository mainActivityRepo = new MainActivityRepository();
        String[] testvalue = mainActivityRepo.getGenreIds(season, womans);
        String expected = "5";
        assertEquals(expected, testvalue[3]);
    }
    @Test
    public void getGenreIdsWomensSs() throws Exception{
        int season = 0;
        boolean womans = true;

        MainActivityRepository mainActivityRepo = new MainActivityRepository();
        String[] testvalue = mainActivityRepo.getGenreIds(season, womans);
        String expected = "1";
        assertEquals(expected, testvalue[1]);
    }
    @Test
    public void getGenreIdsWomensFw() throws Exception{
        int season = 1;
        boolean womans = true;

        MainActivityRepository mainActivityRepo = new MainActivityRepository();
        String[] testvalue = mainActivityRepo.getGenreIds(season, womans);
        String expected = "5";
        assertEquals(expected, testvalue[5]);
    }
    @Test
    public void LastGenreId() throws Exception{
        int season = 1;
        boolean womans = true;

        MainActivityRepository mainActivityRepo = new MainActivityRepository();
        String[] testvalue = mainActivityRepo.getGenreIds(season, womans);
        String expected = "14";
        assertEquals(expected, testvalue[14]);
    }

    /**
     * Since in-memory RealmConfiguration cannot load from asset(pre-populated Realm), JSON import
     * is the only way to construct in-memory pre-populated Realm.
     * This code is the same thing used to pre-populate Realm in the first place.
     */
    private class JsonImportTransaction implements Realm.Transaction{
        @Override
        public void execute(Realm realm) {
            try {
                InputStream inputStream = testContext.getResources().openRawResource(R.raw.genre);
                realm.createAllFromJson(Genre.class, inputStream);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                InputStream inputStream = testContext.getResources().openRawResource(R.raw.style);
                realm.createAllFromJson(Style.class, inputStream);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}