package com.app.i.quattro.threadslist.presenter;

import com.app.i.quattro.threadslist.data.MainActivityRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.Assert;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Presenter is solely responsible for storing values here and doing that at initialization is all that matters
 */
@RunWith(MockitoJUnitRunner.class)
public class MainActivityPresenterTest {
    public MainActivityRepository mockMainActivityRepo = mock(MainActivityRepository.class);
    // tmp values just to fill parameters
    public static final int season = 0;
    public static final boolean womans = true;

    public static final String[] tabTitlesArray = {"title0", "title1"};
    public static final String[] genreIdsArray = {"id0", "id1"};

    /**
     * Whitebox test for MainActivityPresenter
     */
    @Before
    public void setup() throws Exception {
        when(mockMainActivityRepo.getTabTitles(season, womans)).thenReturn(tabTitlesArray);
        when(mockMainActivityRepo.getGenreIds(season, womans)).thenReturn(genreIdsArray);
    }

    @Test
    public void testInitializationGetTabTitles() throws Exception {
        MainActivityPresenter mainActivityPresenter = new MainActivityPresenter();
        mainActivityPresenter.initialize(mockMainActivityRepo);
        String expectedTabTitle = tabTitlesArray[0];
        String result = mainActivityPresenter.tabTitles[0];
        verify(mockMainActivityRepo).getTabTitles(season, womans);
        Assert.assertEquals(expectedTabTitle, result);
    }
    @Test
    public void testInitializationGetGenreIds() throws Exception {
        MainActivityPresenter mainActivityPresenter = new MainActivityPresenter();
        mainActivityPresenter.initialize(mockMainActivityRepo);
        String expectedGenreId = genreIdsArray[0];
        String result = mainActivityPresenter.genreIds[0];
        verify(mockMainActivityRepo).getGenreIds(season, womans);
        Assert.assertEquals(expectedGenreId, result);
    }
}
