package com.app.i.quattro.threadslist;

import com.app.i.quattro.threadslist.data.MainActivityRepository;
import com.app.i.quattro.threadslist.presenter.GalleryFragmentPresenter;
import com.app.i.quattro.threadslist.presenter.MainActivityPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Kei on 2018/01/28.
 */

@Module
public class mockDIModules extends DIModules{
    private static final String[] mockGenreIds = {"id0", "id1"};
    private static final String[] mockTabTitles = {"title0", "title1"};

    private static final String[] mockStyle = {"Style1", "Style2", "Style3"};
    private static final String mockExtraAttrib = "mockExtraAttribute";

    @Provides
    @Override
    public MainActivityPresenter provideMainActivityPresenter(){
        return mock(MainActivityPresenter.class);
    }
    @Provides
    @Override
    public GalleryFragmentPresenter provideGalleryFragmentPresenter(){
        return mock(GalleryFragmentPresenter.class);
    }
    @Provides
    @Singleton
    public MainActivityRepository provideMainActivityRepository(){
        MainActivityRepository mockMainActivityRepository = mock(MainActivityRepository.class);
        when(mockMainActivityRepository.getGenreIds(anyInt(), anyBoolean())).thenReturn(mockGenreIds);
        when(mockMainActivityRepository.getTabTitles(anyInt(), anyBoolean())).thenReturn(mockTabTitles);
        when(mockMainActivityRepository.getStyle(anyString(), anyInt(), anyBoolean())).thenReturn(mockStyle);
        when(mockMainActivityRepository.getExtraAttribute(anyString())).thenReturn(mockExtraAttrib);
        return mockMainActivityRepository;
    }
}
