package com.app.i.quattro.threadslist.view.fragment;

import android.widget.Button;

import com.app.i.quattro.threadslist.BuildConfig;
import com.app.i.quattro.threadslist.TestApplication;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static junit.framework.Assert.assertNotNull;
import static org.robolectric.shadows.support.v4.SupportFragmentTestUtil.startFragment;


/**
 * At the moment I couldn't self teach how MVP should be done properly on Android and my
 * primary focus was to at least implement the idea of it to the best of my ability(view dumb as
 * possible, less clutter in Activity/Fragment, independently testable etc).
 *
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class,
        application = TestApplication.class)
public class GalleryFragmentTest {
    private static final int pageNumber = 1;
    private static final String testTabTitle = "All item";
    private static final String testGenreId = "0";

    Button sortColorButton;

    @Before
    public void setup() throws Exception {
        //sortColorButton = (Button) galleryFragment.findViewById(R.id.sort_color_button);
    }
    @Test
    public void allItemTabTest() throws Exception {
        GalleryFragment galleryFragment = GalleryFragment.newInstance(pageNumber, testTabTitle, testGenreId);
        startFragment(galleryFragment);
        assertNotNull(galleryFragment);
    }

}
