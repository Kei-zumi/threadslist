package com.app.i.quattro.threadslist.presenter;

import com.app.i.quattro.threadslist.data.MainActivityRepository;

import dagger.Module;
import dagger.Provides;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Donno if mocking all of components is the right way
 */

@Deprecated
@Module
public class MockMainActivityPresenter extends MainActivityPresenter {
    private static final String[] testGenreIds = {"id0", "id1"};
    private static final String[] testTabTitles = {"title0", "title1"};

    @Override
    public void initialize(MainActivityRepository mainActivityRepository){
//      Repo access removed
    }

    @Provides
    public MainActivityPresenter provideMainActivityPresenter(){
        MainActivityPresenter mock = mock(MainActivityPresenter.class);
        when(mock.genreIds).thenReturn(testGenreIds);
        when(mock.tabTitles).thenReturn(testTabTitles);
        return mock;
    }
}