package com.app.i.quattro.threadslist;

import android.content.Context;


/**
 * DI, logger and other initialization stuff are taken care of here
 * Created by Kei on 2017/05/31.
 */

public class TestApplication extends MainApplication {
    private static final String LOG_TAG = "TestApplication";
    public static AppComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        component = buildComponent();
    }

    @Override
    public void realmStuff(Context context) {
//       Empty on purpose to avoid executing Realm codes that doesn't work with Robolectric
    }

    public AppComponent getComponent() {
        return component;
    }

    protected AppComponent buildComponent(){
        return DaggerAppComponent.builder()
                .dIModules(new mockDIModules())
                .build();
    }
}
