package com.app.i.quattro.threadslist.presenter;

import com.app.i.quattro.threadslist.data.MainActivityRepository;
import com.app.i.quattro.threadslist.presenter.GalleryFragmentPresenter;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import static org.mockito.Mockito.*;

/**
 * Presenter is solely responsible for storing values here and doing that at initialization is all that matters
 */
@RunWith(MockitoJUnitRunner.class)
public class GalleryFragmentPresenterTest {
    public MainActivityRepository mockMainActivityRepo = mock(MainActivityRepository.class);
    // tmp values just to fill parameters
    public static final String genreId = "test";
    public static final int season = 0;
    public static final boolean womans = true;
    public static final String[] styleArray = {"style0", "style1"};
    public static final String extraAttributeName = "ExtraAttrib";

    /**
     * Whitebox test for GalleryFragmentPresenter
     */
    @Before
    public void setup() throws Exception {
        when(mockMainActivityRepo.getStyle(genreId, season, womans)).thenReturn(styleArray);
        when(mockMainActivityRepo.getExtraAttribute(genreId)).thenReturn(extraAttributeName);
    }

    @Test
    public void testInitializationGetStyle() throws Exception{
        GalleryFragmentPresenter galleryFragPresenter = new GalleryFragmentPresenter();
        galleryFragPresenter.initialize(mockMainActivityRepo, genreId, season, womans);
        String expectedStyle = styleArray[0];
        String result = galleryFragPresenter.style[0];
        verify(mockMainActivityRepo).getStyle(genreId, season, womans);
        Assert.assertEquals(expectedStyle, result);
    }
    @Test
    public void testInitializationGetExtraAttrib() throws Exception{
        GalleryFragmentPresenter galleryFragPresenter = new GalleryFragmentPresenter();
        galleryFragPresenter.initialize(mockMainActivityRepo, genreId, season, womans);
        String result = galleryFragPresenter.extraAttributeName;
        verify(mockMainActivityRepo).getExtraAttribute(genreId);
        Assert.assertEquals(extraAttributeName, result);
    }
}
