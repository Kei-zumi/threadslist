package com.app.i.quattro.threadslist.data;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import static org.mockito.Mockito.mock;

/**
 * Values are within mock presenters so this empty repo is just a placeholder and should do nothing
 */

@Deprecated
@Module
public class MockMainActivityRepository extends MainActivityRepository{
    @Provides
    @Singleton
    public MainActivityRepository provideMainActivityRepository(){
        return mock(MainActivityRepository.class);
    }
}