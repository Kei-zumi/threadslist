package com.app.i.quattro.threadslist.presenter;

import com.app.i.quattro.threadslist.data.MainActivityRepository;

import org.mockito.Mockito;

import dagger.Module;
import dagger.Provides;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Donno if mocking all of components is the right way
 */

@Deprecated
@Module
public class MockGalleryFragmentPresenter extends GalleryFragmentPresenter {
    private static final String[] mockStyle = {"Style1", "Style2", "Style3"};
    private static final String mockExtraAttrib = "mockExtraAttribute";

    @Override
    public void initialize(MainActivityRepository mainActivityRepository, String genreId, int season, boolean womans){
//        Repo access removed
    }

    @Provides
    public GalleryFragmentPresenter provideGalleryFragmentPresenter(){
        GalleryFragmentPresenter mock = mock(GalleryFragmentPresenter.class);
        when(mock.style).thenReturn(mockStyle);
        when(mock.extraAttributeName).thenReturn(mockExtraAttrib);
        return mock;
    }
}