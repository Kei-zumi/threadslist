package com.app.i.quattro.threadslist.view.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import com.android.colorpicker.ColorPickerDialog;
import com.app.i.quattro.threadslist.MainApplication;
import com.app.i.quattro.threadslist.R;
import com.app.i.quattro.threadslist.data.MainActivityRepository;
import com.app.i.quattro.threadslist.presenter.GalleryFragmentPresenter;

import javax.inject.Inject;


import static com.app.i.quattro.threadslist.R.drawable.neg_sign;

public class GalleryFragment extends Fragment {
    private static final String LOGTAG = "GalleryFragment";
    public static final String ARG_PAGE = "ARG_PAGE";  //Bundle str key
    public static final String TAB_TITLE = "TAB_TITLE";
    public static final String GENRE_ID = "GENRE_ID";
    private int nPage;

    // TEMPORARY VALUE FOR PRESENTER INITIALIZATION
    public static final int season = 0;
    public static final boolean womans = true;

    private View view;
    private RelativeLayout layout;

    @Inject
    public MainActivityRepository mRepository;
    @Inject
    public GalleryFragmentPresenter mGalleryFragmentPresenter;

//    TODO color picker dialog stuff
//    public static final String SELECTED_COLOR = "SELECTED_COLOR";
//    public static final int neg_sign = R.drawable.neg_sign;
//    private String str_selected_color = null;
    private int selected_color = -1249799;  // special value for init state & picker default "all color" selection

    public static GalleryFragment newInstance(int position, String tabTitle, String genreId) {
        GalleryFragment frag = new GalleryFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, position);
        args.putString(TAB_TITLE, tabTitle);
        args.putString(GENRE_ID, genreId);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        nPage = getArguments().getInt(ARG_PAGE);
        String genreIdForInit = getArguments().getString(GENRE_ID);
        ((MainApplication)getContext().getApplicationContext()).getComponent().inject(this);
        mGalleryFragmentPresenter.initialize(mRepository, genreIdForInit, season, womans);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        // for dp to px conversion
        final float scale = this.getContext().getResources().getDisplayMetrics().density;

        if (nPage == 1) {
            view = inflater.inflate(R.layout.gallery_frag_all_item_tab, container, false);
        }
        else {
            view = inflater.inflate(R.layout.gallery_fragment, container, false);
            addNormalTabWidgets(view, scale);
        }
        // Sort Color button stuff
        Button sortColorButton = (Button) view.findViewById(R.id.sort_color_button);
        sortColorButton.setOnClickListener(new ColorButtonOnClickListener(sortColorButton));
        // Fit Spinner stuff
        Spinner fitSpinner = (Spinner) view.findViewById(R.id.fit_spinner);
        SpinnerAdapter fitSpinnerAdapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.fit_order, R.layout.spinner_item_style_sort);
        fitSpinner.setAdapter(fitSpinnerAdapter);
        fitSpinner.setOnItemSelectedListener(new fitOnItemSelectedListener());
        // Sort Spinner stuff
        SpinnerAdapter sortSpinnerAdapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.sort_order, R.layout.spinner_item_style_sort);
        Spinner sortSpinner = (Spinner) view.findViewById(R.id.sort_spinner);
        sortSpinner.setAdapter(sortSpinnerAdapter);
        sortSpinner.setOnItemSelectedListener(new sortOnItemSelectedListener());
        // Extra attribute Spinner stuff
        if(mGalleryFragmentPresenter.extraAttributeName!=null) {
            addExtraAttributeSpinner(view, scale, mGalleryFragmentPresenter.extraAttributeName);
        }
        Log.i(LOGTAG, "onCreateView, nPage = " + nPage);
        return view;
    }
    /**
     * Programmatically add Style spinner to tab layout.
     * @param view View to add widgets to
     * @param scale Screen scale info used for DP to PX conversion
     * @return View
     */
    private void addNormalTabWidgets(View view, float scale){
        int thirty_five_dp_in_px = (int) (35 * scale + 0.5f);
        int five_dp_in_px = (int) (5 * scale + 0.5f);
        layout = (RelativeLayout) view.findViewById(R.id.gallery_fragment);
        // Style spinner stuff
        Spinner styleSpinner = new Spinner(getContext());
        styleSpinner.setBackgroundResource(R.drawable.transparent);
        styleSpinner.setPopupBackgroundResource(R.color.colorPrimary);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, thirty_five_dp_in_px);
        layoutParams.addRule(RelativeLayout.ALIGN_LEFT);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        styleSpinner.setLayoutParams(layoutParams);
        layout.addView(styleSpinner);
        ArrayAdapter<String> styleSpinnerAdapter = new ArrayAdapter<>(getActivity(),
                R.layout.spinner_item_style_sort, mGalleryFragmentPresenter.style);
        styleSpinnerAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        styleSpinner.setAdapter(styleSpinnerAdapter);
        styleSpinner.setOnItemSelectedListener(new styleOnItemSelectedListener());
        // Style spinner neg sign stuff
//        ImageView sort_neg_imageview = new ImageView(this.getContext());
//        sort_neg_imageview.setImageResource(neg_sign);
//        RelativeLayout.LayoutParams neg_layout_params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, thirty_five_dp_in_px);
//        neg_layout_params.addRule(RelativeLayout.LEFT_OF, R.id.style_spinner);
//        neg_layout_params.setMargins(0, thirty_five_dp_in_px, five_dp_in_px, 0);
//        sort_neg_imageview.setLayoutParams(neg_layout_params);
//        layout.addView(sort_neg_imageview);
    }

    private void addExtraAttributeSpinner(View view, float scale, String extraAttribName){
        int thirtyFiveDpInPx = (int) (35 * scale + 0.5f);
        int fiveDpInPx = (int) (-5 * scale + 0.5f);
        layout = (RelativeLayout) view.findViewById(R.id.gallery_fragment);

        // Negative ImageView stuff
        ImageView negImageView = new ImageView(this.getContext());
        negImageView.setImageResource(neg_sign);
        negImageView.setId(View.generateViewId());
        RelativeLayout.LayoutParams fit_layout_params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, thirtyFiveDpInPx);
        fit_layout_params.addRule(RelativeLayout.RIGHT_OF, R.id.fit_spinner);
        fit_layout_params.setMargins(0, thirtyFiveDpInPx, fiveDpInPx, 0);
        negImageView.setLayoutParams(fit_layout_params);
        layout.addView(negImageView);

        // Extra Attribute spinner stuff
        int extraAttribId = getResources().getIdentifier(extraAttribName, "array", getActivity().getPackageName());
        int negImgViewId = negImageView.getId();

        SpinnerAdapter extraSpinnerAdapter = ArrayAdapter.createFromResource(getActivity(),
                extraAttribId, R.layout.spinner_item_style_sort);
        Spinner styleSpinner = new Spinner(getContext());
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, thirtyFiveDpInPx);
        layoutParams.addRule(RelativeLayout.RIGHT_OF, negImgViewId);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        styleSpinner.setLayoutParams(layoutParams);
        styleSpinner.setBackground(null);
        layout.addView(styleSpinner);
        styleSpinner.setAdapter(extraSpinnerAdapter);
        styleSpinner.setOnItemSelectedListener(new extraOnItemSelectedListener());
    }

    private class styleOnItemSelectedListener implements AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//            TODO style sort this style listener supposed to do
        }
        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    }
    private class fitOnItemSelectedListener implements AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//            TODO sorting shit this fit listener supposed to do
        }
        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    }
    private class sortOnItemSelectedListener implements AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//            TODO sorting shit this sort listener supposed to do
        }
        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    }
    private class extraOnItemSelectedListener implements AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//            TODO sorting shit this sort listener supposed to do
        }
        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    }
    private class ColorButtonOnClickListener implements View.OnClickListener {
        private Button sortColorButton;

        private ColorButtonOnClickListener(Button sortColorButton) {
            this.sortColorButton = sortColorButton;
        }
        @Override
        public void onClick(View v) {
            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
            // Shitty tmp implementation just to display dialog
            int[] colors = getResources().getIntArray(com.android.colorpicker.R.array.palette_colors);
            ColorPickerDialog colorPickerDialog = new ColorPickerDialog();
            colorPickerDialog.initialize(R.string.picker_title, colors, selected_color, 4);
//            colorPickerDialog.setOnColorSelectedListener(new ColorPickerSwatch.OnColorSelectedListener() {
//                @Override
//                public void onColorSelected(int color) {
//                    selected_color = color;
//                    int index = find(color_array, color);
//                    str_selected_color = str_color_array[index];
//                    sortColorButton.setText(str_selected_color);
//                    if (selected_color == -1249799){
//                        GradientDrawable neg_drawable = (GradientDrawable) ResourcesCompat.getDrawable(getResources(), neg_sign, null);
//                        sortColorButton.setCompoundDrawablesWithIntrinsicBounds(neg_drawable, null, null, null);
//                    }
//                    else {
//                        GradientDrawable color_dot = (GradientDrawable) ResourcesCompat.getDrawable(getResources(), R.drawable.color_dot, null);
//                        color_dot.setColor(color);
//                        sortColorButton.setCompoundDrawablesWithIntrinsicBounds(color_dot, null, null, null);
//                    }
//                }
//            });
            colorPickerDialog.show(ft, "PickerDialog");
        }
    }

//    TODO change colorpicker module to have this function built in
    public int find(int[] array, int value) {
        int result = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == value) {
                result = i;
            }
        }
        return result;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
//        outState.putInt(SELECTED_COLOR, selected_color);
    }


//     ImageView fit_neg_imageview = new ImageView(this.getContext());
//     fit_neg_imageview.setImageResource(neg_sign);
//     RelativeLayout.LayoutParams fit_layout_params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, thirty_five_dp_in_px);
//     fit_layout_params.addRule(RelativeLayout.LEFT_OF, R.id.fit_spinner);
//     fit_layout_params.setMargins(0, thirty_five_dp_in_px, five_dp_in_px, 0);
//     fit_neg_imageview.setLayoutParams(fit_layout_params);
//     layout.addView(fit_neg_imageview);
//
//     if (nPage != 1) {
//     TypedArray style_multi_array = getResources().obtainTypedArray(R.array.style_array);
//     SpinnerAdapter styleSpinnerAdapter = ArrayAdapter.createFromResource(this.getActivity(),
//     style_multi_array.getResourceId(nPage-2, -1), R.layout.spinner_item_style_sort);
//     style_multi_array.recycle();
//     Spinner styleSpinner = new Spinner(getContext());
//     RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, thirty_five_dp_in_px);
//     layoutParams.addRule(RelativeLayout.ALIGN_LEFT);
//     layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
//     styleSpinner.setLayoutParams(layoutParams);
//     layout.addView(styleSpinner);
//     styleSpinner.setBackgroundResource(R.drawable.rectangle_frame);
//     styleSpinner.setPopupBackgroundResource(R.color.colorPrimary);
//     styleSpinner.setAdapter(styleSpinnerAdapter);
//     styleSpinner.setOnItemSelectedListener(new styleOnItemSelectedListener());
//     }
//     TODO clever way to add same 2 imageview to each of these spinner
//
//     ImageView sort_neg_imageview = new ImageView(this.getContext());
//     sort_neg_imageview.setImageResource(neg_sign);
//     RelativeLayout.LayoutParams sort_layout_params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, thirty_five_dp_in_px);
//     sort_layout_params.addRule(RelativeLayout.LEFT_OF, R.id.sort_spinner);
//     sort_layout_params.setMargins(0, thirty_five_dp_in_px, five_dp_in_px, 0);
//     sort_neg_imageview.setLayoutParams(sort_layout_params);
//     layout.addView(sort_neg_imageview);
//
//     Button sortColorButton = (Button) view.findViewById(R.id.sort_color_button);
//     sortColorButton.setOnClickListener(new ColorButtonOnClickListener(sortColorButton));
//     if (selected_color == -1249799){
//     GradientDrawable neg_drawable = (GradientDrawable) ResourcesCompat.getDrawable(getResources(), neg_sign, null);
//     sortColorButton.setCompoundDrawablesWithIntrinsicBounds(neg_drawable, null, null, null);
//     sortColorButton.setText(R.string.color_button);
//     }
//     else {
//     GradientDrawable color_dot = (GradientDrawable) ResourcesCompat.getDrawable(getResources(), R.drawable.color_dot, null);
//     color_dot.setColor(selected_color);
//     sortColorButton.setCompoundDrawablesWithIntrinsicBounds(color_dot, null, null, null);
//     sortColorButton.setText(str_selected_color);
//     }
}