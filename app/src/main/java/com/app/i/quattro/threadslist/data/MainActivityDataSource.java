package com.app.i.quattro.threadslist.data;

/**
 * Created by Kei on 2017/05/29.
 */

public interface MainActivityDataSource {
    String[] getTabTitles(int season, boolean womans);
    String[] getGenreIds(int season, boolean womans);
    String[] getStyle(String id, int season, boolean womans);
    String getExtraAttribute(String id);
}
