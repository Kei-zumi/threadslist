package com.app.i.quattro.threadslist.utils;

import android.util.Log;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Kei on 2017/06/09.
 */

// TODO proper exception handling
@Deprecated
public class Utils {
    private static final String LOG_TAG = "Utils";

    /**
     * Verifies given file using known MD5 hash value.
     * Result is returned as a boolean value
     *
     * @param hash known MD5 hash of genuine file
     * @param path path of file to be verified
     * */
    public static boolean verifyFile(String hash, String path) {
        InputStream is;
        String result = null;
        try {
            is = new FileInputStream(path);
            byte[] md5 = DigestUtils.md5(is);
            char[] hexChars = Hex.encodeHex(md5);
            result = new String(hexChars);
            Log.i(LOG_TAG, path + ", MD5= " + result);
        } catch (IOException e) {
            e.setStackTrace(e.getStackTrace());
            Log.i(LOG_TAG, "verifyFile() IO exception " + e.getMessage());
        }
        return hash.equals(result);
    }
}
