package com.app.i.quattro.threadslist.presenter;


import com.app.i.quattro.threadslist.data.MainActivityRepository;

/**
 * .initialize() must be executed after instantiation or else values remain null.
 * It was meant to avoid passing parameters through constructor so that it's easier to DI into fragment
 */

public class MainActivityPresenter implements MainActivityContract.Presenter {
    private MainActivityRepository mRepository;

    public String[] tabTitles;
    public String[] genreIds;

    // TEMPORARY VALUE FOR DB OPERATIONS
    public static final boolean visible = true;
    public static final int season = 0;
    public static final boolean womans = true;

//    Empty constructor
    public MainActivityPresenter() {
    }

    /**
     * Invoke methods which assign values to field
     */
    public void initialize(MainActivityRepository mainActivityRepository){
        this.mRepository = mainActivityRepository;
        tabTitles = getTabTitles(season, womans);
        genreIds = getGenreIds(season, womans);
    }

    public String[] getTabTitles(int season, boolean womans){
//      TODO invoke View to pass this info?
        return mRepository.getTabTitles(season, womans);
    }

    public String[] getGenreIds(int season, boolean womans){
//      TODO invoke View to pass this info?
        return mRepository.getGenreIds(season, womans);
    }

    @Override
    public void start() {
    }
}
