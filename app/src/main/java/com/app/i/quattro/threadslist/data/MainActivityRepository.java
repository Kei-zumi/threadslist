package com.app.i.quattro.threadslist.data;

import com.app.i.quattro.threadslist.realmObj.Genre;
import com.app.i.quattro.threadslist.realmObj.Style;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Concrete implementation of DB as a data source, local source only for now so no extra YAGNI abstraction
 * Created by Kei on 2017/05/25.
 */
public class MainActivityRepository implements MainActivityDataSource {

    private static final String LOG_TAG = "MainActivityRepository";

//  Empty constructor
    public MainActivityRepository() {
    }
    /**
     *  Used only by MainActivity presenter.
     *  Query for all Genre item where "visible" is true.
     *  Item 0 in Array is manually added to deal with special "All item" tab.
     *  Currently item 0 is loaded from hard coded string.
     *  Query MUST be identical to getGenreIds()
     *
     *  Side note: Yes it's redundant, sucks to test, and it should be one method returning
     *  genre ID and tab title in an object or sth but managing such object is beyond my capabilities atm
     *  @return String Array ordered by "sort_order"
     */
    @Override
    public String[] getTabTitles(int season, boolean womans) {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Genre> realmResult = realm.where(Genre.class)
                .equalTo("visible", true)
                .beginGroup()
                .equalTo("womans", false)
                .or()
                .equalTo("womans", womans)
                .endGroup()
                .beginGroup()
                .equalTo("season", season)
                .or()
                .equalTo("season", 3)
                .endGroup()
                .findAllSorted("sort_order");
//      POS tmp implementation of Realm to ArrayList conversion
        String[] result = new String[realmResult.size() + 1];
        result[0] = "All item";
        for(int i=0; i<realmResult.size();i++){
//          TODO maybe query String from R based on name in DB for localization?
            result[i + 1] = realmResult.get(i).getGenre_name();
        }
        return result;
    }

    /**
     * Used only by MainActivity presenter.
     * Item 0 in Array is manually added to deal with special "All item" tab.
     * Currently item 0 is loaded from hard coded string.
     * Season group always retrieves all-season items AND items for given season
     * Query MUST be identical to getTabTitles()
     * @return int Array ordered by "sort_order"
     */
    public String[] getGenreIds(int season, boolean womans){
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Genre> realmResult = realm.where(Genre.class)
                .equalTo("visible", true)
                .beginGroup()
                .equalTo("womans", false)
                .or()
                .equalTo("womans", womans)
                .endGroup()
                .beginGroup()
                .equalTo("season", season)
                .or()
                .equalTo("season", 3)
                .endGroup()
                .findAllSorted("sort_order");
        String[] result = new String[realmResult.size() + 1];
        result[0] = "All item";
        for(int i=0; i<realmResult.size();i++){
//          TODO maybe query String from R based on name in DB for localization?
            result[i+1] = realmResult.get(i).getGenre_id();
        }
        return result;
    }

    /**
     * Used by GalleryFragment presenter to load Style Spinner.
     * Item 0 in Array is manually added to deal with default value "Style" with nothing selected.
     * Currently item 0 is loaded from hard coded string.
     * @param id Genre ID which the style belongs to
     * @param season to load season specific style along with those for all-season
     * @param womans to load women's item along with load men's item which is always loaded
     * @return String Array ordered by "sort_order"
     */
    public String[] getStyle(String id, int season, boolean womans){
        Realm realm = Realm.getDefaultInstance();

        RealmResults<Style> realmResult = realm.where(Style.class)
                .equalTo("genre_id", id)
                .equalTo("visible", true)
                .beginGroup()
                .equalTo("womans", false)
                .or()
                .equalTo("womans", womans)
                .endGroup()
                .beginGroup()
                .equalTo("season", season)
                .or()
                .equalTo("season", 3)
                .endGroup()
                .findAllSorted("sort_order");
        String[] result = new String[realmResult.size() + 1];
        result[0] = "Style";
        for(int i=0; i<realmResult.size();i++){
//          TODO maybe query String from R based on name in DB for localization?
            result[i+1] = realmResult.get(i).getStyle_name();
        }
        return result;
    }

    /**
     * @param id genre_id to query for
     * @return String of extra attribute or "None"
     */
    public String getExtraAttribute(String id){
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Genre> realmResult = realm.where(Genre.class)
                .equalTo("genre_id", id)
                .findAll();
        if(realmResult.size() == 1){
            return realmResult.get(0).getExtra_attribute_name();
        }
        else{
            return null;
        }
    }
}
