package com.app.i.quattro.threadslist.realmObj;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Kei on 2017/05/08.
 */


public class Style extends RealmObject {
    @PrimaryKey
    private String style_id = UUID.randomUUID().toString();
    private String style_name;
    private String genre_id;
    private boolean visible;
    private boolean womans;
    private int season;
    private int sort_order;

    public String getStyle_id() {
        return style_id;
    }

    public void setStyle_id(String style_id) {
        this.style_id = style_id;
    }

    public String getStyle_name() {
        return style_name;
    }

    public void setStyle_name(String style_name) {
        this.style_name = style_name;
    }

    public String getGenre_id() {
        return genre_id;
    }

    public void setGenre_id(String genre_id) {
        this.genre_id = genre_id;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isWomans() {
        return womans;
    }

    public void setWomans(boolean womans) {
        this.womans = womans;
    }

    public int getSeason() {
        return season;
    }

    public void setSeason(int season) {
        this.season = season;
    }

    public int getSort_order() {
        return sort_order;
    }

    public void setSort_order(int sort_order) {
        this.sort_order = sort_order;
    }
}
