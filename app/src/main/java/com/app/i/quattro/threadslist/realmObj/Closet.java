package com.app.i.quattro.threadslist.realmObj;


import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


/**
 * Created by Kei on 2017/05/08.
 */


public class Closet extends RealmObject{
    @PrimaryKey
    private String closet_id;
    private String closet_name;

    public String getCloset_id() {
        return closet_id;
    }
    public void setCloset_id(String closet_id) {
        this.closet_id = closet_id;
    }
    public String getCloset_name() {
        return closet_name;
    }
    public void setCloset_name(String closet_name) {
        this.closet_name = closet_name;
    }
}
