package com.app.i.quattro.threadslist;

import com.app.i.quattro.threadslist.view.activity.MainActivity;
import com.app.i.quattro.threadslist.view.fragment.GalleryFragment;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Kei on 2017/08/22.
 */

@Singleton
@Component(modules = DIModules.class)
public interface AppComponent {
        void inject(MainActivity mainActivity);
        void inject(GalleryFragment galleryFragment);
}