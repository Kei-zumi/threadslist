package com.app.i.quattro.threadslist.presenter;


import android.util.Log;

import com.app.i.quattro.threadslist.data.MainActivityRepository;
import com.app.i.quattro.threadslist.view.GalleryContract;
/**
 * .initialize() must be executed after instantiation or else values remain null.
 * It was meant to avoid passing parameters through constructor so that it's easier to DI into fragment
 */

public class GalleryFragmentPresenter implements GalleryContract.Presenter {
    private static final String LOG_TAG = "GalleryFragPresenter";
    private MainActivityRepository mRepository;

//  TODO tmp values which are supposed to come from shared pref
    public String[] style;
    public String extraAttributeName;

//    Empty constructor
    public GalleryFragmentPresenter() {
    }

    public void initialize(MainActivityRepository mainActivityRepository, String genreId, int season, boolean womans){
        this.mRepository = mainActivityRepository;
        style = mRepository.getStyle(genreId, season, womans);
        extraAttributeName = mRepository.getExtraAttribute(genreId);
    }

    @Override
    public void start() {
    }
}
