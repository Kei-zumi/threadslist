package com.app.i.quattro.threadslist.presenter;

import com.app.i.quattro.threadslist.BasePresenter;
import com.app.i.quattro.threadslist.BaseView;
import com.app.i.quattro.threadslist.view.GalleryContract;

import java.util.List;

/**
 * Created by Kei on 2017/05/26.
 */

public interface MainActivityContract {
    interface View extends BaseView<GalleryContract.Presenter> {
    }

    interface Presenter extends BasePresenter {
    }
}
