package com.app.i.quattro.threadslist.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.app.i.quattro.threadslist.R;
import com.app.i.quattro.threadslist.utils.Utils;

import org.apache.commons.io.IOUtils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by Kei on 2017/05/31.
 */

@Deprecated
public class MyOpenHelper {
    private static final String LOG_TAG = "MyOpenHelper";

    private Context mContext;

    private final String mName;
    private final String mDatabasePath;

    //private SQLiteDatabase db = null;

    public MyOpenHelper(Context context, String name) {
        // Not sure if this mContext is ok but its working to pass context to onCreate()
        mContext = context;
        mName = name;
        mDatabasePath = context.getDatabasePath(mName).getPath();
        //this.getDatabase();
    }
    /**
    @Override
    public void onCreate(Database db) {
        super.onCreate(db);

        String dest = mDatabasePath;
        InputStream is;
        Log.i(LOG_TAG, "dest path: " + dest);
        try {
            is = mContext.getAssets().open(mName);
            try {
                OutputStream os = new FileOutputStream(dest);
                IOUtils.copy(is, os);
                IOUtils.closeQuietly(os);
            } catch (IOException e) {
                e.setStackTrace(e.getStackTrace());
                Log.i(LOG_TAG, "Unable to write " + dest + " to data directory" + e.getMessage());
//          TODO throw exception for ""Unable to write " + dest + " to data directory"", e.setStackTrace(e.getStackTrace());
            }
        } catch (IOException e2) {
            e2.setStackTrace(e2.getStackTrace());
            Log.i(LOG_TAG, "Missing asset file " + e2.getMessage());
            //IOException exception = new IOException("Missing file");
            //exception.setStackTrace(exception.getStackTrace());
        }
        if(!Utils.verifyFile(ASSET_DB_MD5, dest)){
            Log.i(LOG_TAG, "DB copying failed");
            //throw new IllegalStateException("DB corrupt while copying");
        }
        // should be some this.db overwrite shit
    }

    @Override
    public void onUpgrade(Database db, int oldVersion, int newVersion) {
    }

    /*
    public SQLiteDatabase getDatabase() {
        if (this.db == null) {
            this.db = this.getWritableDatabase();
        }
        return this.db;
    }
*/
}
