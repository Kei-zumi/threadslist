package com.app.i.quattro.threadslist.utils;

import android.content.res.Resources;

import android.util.Log;

import com.app.i.quattro.threadslist.R;
import com.app.i.quattro.threadslist.realmObj.Genre;

import java.io.InputStream;

import io.realm.Realm;

/**
 * Created by Kei on 2017/07/17.
 */

@Deprecated
public class RealmImporter {
    private static final String LOG_TAG = "RealmImporter";
    private Resources resources;

    public RealmImporter(Resources resources) {
        this.resources = resources;
    }

    public void importFromJson() {
        Realm realm = Realm.getDefaultInstance();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                try {
                    InputStream inputStream = resources.openRawResource(R.raw.genre);
                    realm.createAllFromJson(Genre.class, inputStream);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    realm.close();
                }
            }
        });
        Log.d(LOG_TAG, "createAllFromJson Task completed");
    }
}
