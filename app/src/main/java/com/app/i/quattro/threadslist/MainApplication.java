package com.app.i.quattro.threadslist;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.app.i.quattro.threadslist.data.MainActivityRepository;
import com.app.i.quattro.threadslist.presenter.GalleryFragmentPresenter;
import com.app.i.quattro.threadslist.presenter.MainActivityPresenter;
import com.app.i.quattro.threadslist.realmObj.Genre;
import com.app.i.quattro.threadslist.realmObj.Style;

import java.io.InputStream;

import io.realm.Realm;
import io.realm.RealmConfiguration;


/**
 * DI, logger and other initialization stuff are taken care of here
 * Created by Kei on 2017/05/31.
 */

public class MainApplication extends Application {
    private static final String LOG_TAG = "MainApplication";
    public static AppComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        realmStuff(this);
        component = buildComponent();
//      TODO Debugger init
    }

    public AppComponent getComponent() {
        return component;
    }

    /**
     * Realm initialization executed one time when Realm DB is nonexistent.
     * It's a separate method to allow overriding when test Application class extends it
     * @param context context
     */
    public void realmStuff(Context context){
        Realm.init(context);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .assetFile("default.realm")
                .schemaVersion(0)
//                .name("closetdb.realm")
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);
    }

    /**
    *  Import "Genre" and "Style" data from raw JSON
    * */
    @Deprecated
    public class JsonImportTransaction implements Realm.Transaction{
//      TODO preferably the app should be shipped with pre-populated Realm DB
        @Override
        public void execute(Realm realm) {
//          shitty implementation justified since its for one time use and small data size
            try {
                InputStream inputStream = getResources().openRawResource(R.raw.genre);
                realm.createAllFromJson(Genre.class, inputStream);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                realm.close();
            }
            try {
                InputStream inputStream = getResources().openRawResource(R.raw.style);
                realm.createAllFromJson(Style.class, inputStream);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                realm.close();
            }
            Log.d(LOG_TAG, "createAllFromJson successful");
            Log.d(LOG_TAG, "path: " + realm.getPath());
        }
    }

    protected AppComponent buildComponent(){
        return DaggerAppComponent.builder()
                .dIModules(new DIModules())
                .build();
    }
}
