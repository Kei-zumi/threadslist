package com.app.i.quattro.threadslist;

import com.app.i.quattro.threadslist.data.MainActivityRepository;
import com.app.i.quattro.threadslist.presenter.GalleryFragmentPresenter;
import com.app.i.quattro.threadslist.presenter.MainActivityPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Kei on 2018/01/28.
 */

@Module
public class DIModules {
    @Provides
    public MainActivityPresenter provideMainActivityPresenter(){
        return new MainActivityPresenter();
    }
    @Provides
    public GalleryFragmentPresenter provideGalleryFragmentPresenter(){
        return new GalleryFragmentPresenter();
    }
    @Provides
    @Singleton
    public MainActivityRepository provideMainActivityRepository(){
        return new MainActivityRepository();
    }
}
