package com.app.i.quattro.threadslist.realmObj;

/**
 * Created by Kei on 2017/05/01.
 */

@Deprecated
public class SortObject {
    private String[] season;
    private String[] sort;
    private String[] fit;
    private String[] length;

    public SortObject(){
        /**
        this.season = itemAttributeRepository.season();
        this.sort = itemAttributeRepository.sort();
        this.fit = itemAttributeRepository.fit();
        this.length = itemAttributeRepository.length();
         */
    }

    public String[] getSeason() {
        return season;
    }

    public String[] getSort() {
        return sort;
    }

    public String[] getFit() {
        return fit;
    }

    public String[] getLength() {
        return length;
    }
}
