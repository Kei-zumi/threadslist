package com.app.i.quattro.threadslist.view.activity;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Toast;

import com.app.i.quattro.threadslist.MainApplication;
import com.app.i.quattro.threadslist.presenter.MainActivityPresenter;
import com.app.i.quattro.threadslist.R;
import com.app.i.quattro.threadslist.data.MainActivityRepository;
import com.app.i.quattro.threadslist.realmObj.Style;
import com.app.i.quattro.threadslist.view.fragment.GalleryFragment;
import com.facebook.stetho.Stetho;
import com.uphyca.stetho_realm.RealmInspectorModulesProvider;

import javax.inject.Inject;

import io.realm.Realm;

/**
 * https://github.com/googlesamples/android-architecture/blob/todo-mvp/todoapp/app/src/main/java/com/example/android/architecture/blueprints/todoapp/taskdetail/TaskDetailActivity.java
 * based off of this
 */

public class MainActivity extends AppCompatActivity {
    private int[] color_array;
    private String[] str_color_array;

    private static final String LOGTAG = "MainActivity";

    public String[] tabTitleGenres;
    public String[] genreIdArray;
    public int page_count;

    @Inject
    public MainActivityRepository mMainActivityRepository;
    @Inject
    public MainActivityPresenter mMainActivityPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Stetho.initialize(Stetho.newInitializerBuilder(this)
                .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                .enableWebKitInspector(RealmInspectorModulesProvider.builder(this).build())
                .build());

        ((MainApplication)this.getApplicationContext()).getComponent().inject(this);

        // Set up presenter & get info
        mMainActivityPresenter.initialize(mMainActivityRepository);
        tabTitleGenres = mMainActivityPresenter.tabTitles;
        genreIdArray = mMainActivityPresenter.genreIds;
        page_count = tabTitleGenres.length;

        // set up the tool bar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // set up mode spinner
        SpinnerAdapter spinnerAdapter = ArrayAdapter.createFromResource(getApplicationContext(),
                R.array.mode_spinner, R.layout.spinner_item_toolbar);
        Spinner modeSpinner = (Spinner) findViewById(R.id.mode_spinner);
        modeSpinner.setAdapter(spinnerAdapter);
        modeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//          TODO Case statement here to invoke presenter
                Toast.makeText(MainActivity.this,
                        "onItemSelected:" + position,
                        Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Realm realm = Realm.getDefaultInstance();
//                realm.beginTransaction();
//                Style style = realm.createObject(Style.class, null);
//                style.setStyle_name("Tops");
//                style.setGenre_id("tmp");
//                style.setVisible(true);
//                style.setWomans(false);
//                style.setSeason(1);
//                style.setSort_order(1);
//                realm.commitTransaction();
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
//              TODO launch camera activity via presenter
            }
        });

        // Get the ViewPager & set its PagerAdapter so that it can display items
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        PagerAdapter pagerAdapter = new ItemTabFragmentPagerAdapter(getSupportFragmentManager(),
                MainActivity.this);
        viewPager.setAdapter(pagerAdapter);
        //Pass the TabLayout to the ViewPager
        TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent();
            intent.setClassName(this, "com.app.i.quattro.threadslist.view.activity.PreferenceActivity");
            startActivity(intent);
            return true;
        }
        if (id == R.id.action_about) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public class ItemTabFragmentPagerAdapter extends FragmentPagerAdapter {
        private Context context;

        public ItemTabFragmentPagerAdapter(FragmentManager fm, Context context) {
            super(fm);
            this.context = context;
        }
//      TODO is getCount() needed?
        @Override
        public int getCount() {
            return page_count;
        }

        @Override
        public Fragment getItem(int position) {
            return GalleryFragment.newInstance(position + 1, tabTitleGenres[position], genreIdArray[position]);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            // Generate title from array based on item position
            return tabTitleGenres[position];
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}