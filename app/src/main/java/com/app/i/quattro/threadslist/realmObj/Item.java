package com.app.i.quattro.threadslist.realmObj;

import java.util.Date;
import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by Kei on 2017/05/09.
 */

public class Item extends RealmObject {
    @PrimaryKey
    private String item_id = UUID.randomUUID().toString();
    private String file_name;
    private int closet_id;
    private int genre_id;
    private int style_id;
    private int fit;
    private int season;
    @Required
    private String color_1;
    private String color_2;
    private String color_3;
    private String color_4;
    private boolean favorite;
    private boolean sample;
    private Date date_added;
    private String extra_attribute;

    public String getId() {
        return item_id;
    }

    public void setId(String id) {
        this.item_id = id;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public int getCloset_id() {
        return closet_id;
    }

    public void setCloset_id(int closet_id) {
        this.closet_id = closet_id;
    }

    public int getGenre_id() {
        return genre_id;
    }

    public void setGenre_id(int genre_id) {
        this.genre_id = genre_id;
    }

    public int getStyle_id() {
        return style_id;
    }

    public void setStyle_id(int style_id) {
        this.style_id = style_id;
    }

    public int getFit() {
        return fit;
    }

    public void setFit(int fit) {
        this.fit = fit;
    }

    public int getSeason() {
        return season;
    }

    public void setSeason(int season) {
        this.season = season;
    }

    public String getColor_1() {
        return color_1;
    }

    public void setColor_1(String color_1) {
        this.color_1 = color_1;
    }

    public String getColor_2() {
        return color_2;
    }

    public void setColor_2(String color_2) {
        this.color_2 = color_2;
    }

    public String getColor_3() {
        return color_3;
    }

    public void setColor_3(String color_3) {
        this.color_3 = color_3;
    }

    public String getColor_4() {
        return color_4;
    }

    public void setColor_4(String color_4) {
        this.color_4 = color_4;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public boolean isSample() {
        return sample;
    }

    public void setSample(boolean sample) {
        this.sample = sample;
    }

    public Date getDate_added() {
        return date_added;
    }

    public void setDate_added(Date date_added) {
        this.date_added = date_added;
    }

    public String getExtra_attribute() {
        return extra_attribute;
    }

    public void setExtra_attribute(String extra_attribute) {
        this.extra_attribute = extra_attribute;
    }
}
