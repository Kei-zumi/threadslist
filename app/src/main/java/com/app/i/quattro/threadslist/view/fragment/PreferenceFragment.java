package com.app.i.quattro.threadslist.view.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.Preference;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.app.i.quattro.threadslist.R;

/**
 * Created by Kei on 2017/08/31.
 */

public class PreferenceFragment extends PreferenceFragmentCompat {
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        // Load the Preferences from the XML file
        addPreferencesFromResource(R.xml.preferences);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        view.setBackgroundResource(R.color.colorWhite);
        // set up preference listeners
        Preference manualBackupButton = findPreference("pref_backup_manually");
        manualBackupButton.setOnPreferenceClickListener(new manualBackupOnPreferenceClickListener());
        Preference manualRestoreButton = findPreference("pref_backup_restore_manually");
        manualRestoreButton.setOnPreferenceClickListener(new manualRestoreOnPreferenceClickListener());
        return view;
    }

    private class manualBackupOnPreferenceClickListener implements Preference.OnPreferenceClickListener {
        @Override
        public boolean onPreferenceClick(Preference preference) {
            Toast.makeText(getActivity(),"manualBackupOnPreferenceClickListener", Toast.LENGTH_SHORT).show();
            return true;
        }
    }
    private class manualRestoreOnPreferenceClickListener implements Preference.OnPreferenceClickListener {
        @Override
        public boolean onPreferenceClick(Preference preference) {
            Toast.makeText(getActivity(),"manualRestoreOnPreferenceClickListener", Toast.LENGTH_SHORT).show();
            return true;
        }
    }
}
