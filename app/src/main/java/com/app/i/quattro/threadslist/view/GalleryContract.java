package com.app.i.quattro.threadslist.view;

import com.app.i.quattro.threadslist.BaseView;
import com.app.i.quattro.threadslist.BasePresenter;

/**
 * Created by Kei on 2017/05/24.
 */

public interface GalleryContract {
    interface View extends BaseView<Presenter>{
    }

    interface Presenter extends BasePresenter {
    }

}
