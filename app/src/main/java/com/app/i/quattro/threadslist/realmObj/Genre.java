package com.app.i.quattro.threadslist.realmObj;


import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by Kei on 2017/05/08.
 */

public class Genre extends RealmObject {
    @PrimaryKey
    private String genre_id = UUID.randomUUID().toString();
    private String genre_name;
    private int closet_id;
    private boolean visible;
    private boolean womans;
    private int season;
    private int sort_order;
    private String extra_attribute_name;

    public String getGenre_id() {
        return genre_id;
    }

    public void setGenre_id(String genre_id) {
        this.genre_id = genre_id;
    }

    public String getGenre_name() {
        return genre_name;
    }

    public void setGenre_name(String genre_name) {
        this.genre_name = genre_name;
    }

    public int getCloset_id() {
        return closet_id;
    }

    public void setCloset_id(int closet_id) {
        this.closet_id = closet_id;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isWomans() {
        return womans;
    }

    public void setWomans(boolean womans) {
        this.womans = womans;
    }

    public int getSeason() {
        return season;
    }

    public void setSeason(int season) {
        this.season = season;
    }

    public int getSort_order() {
        return sort_order;
    }

    public void setSort_order(int sort_order) {
        this.sort_order = sort_order;
    }

    public String getExtra_attribute_name() {
        return extra_attribute_name;
    }

    public void setExtra_attribute_name(String extra_attribute_name) {
        this.extra_attribute_name = extra_attribute_name;
    }
}
