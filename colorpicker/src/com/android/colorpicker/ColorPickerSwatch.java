/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.colorpicker;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

/**
 * Creates a circular swatch of a specified color.  Adds a checkmark if marked as checked.
 */
public class ColorPickerSwatch extends FrameLayout implements View.OnClickListener {
    private int mColor;
    private ImageView mSwatchImage;
    private ImageView mCheckmarkImage;
    private OnColorSelectedListener mOnColorSelectedListener;
    public static final String LOGTAG = "ColorPickerSwatch ";

    /**
     * Interface for a callback when a color square is selected.
     */
    public interface OnColorSelectedListener {

        /**
         * Called when a specific color square has been selected.
         */
        public void onColorSelected(int color);
    }

    public ColorPickerSwatch(Context context, int color, boolean checked,
            OnColorSelectedListener listener) {
        /**
         * Swatch is a shape drawable generated when specific color is passed.
         * altered code below use specific color value as a trigger to set a special drawable
         * setColor() makes normal color swatch & dimmed color since dialog can't handle alpha
         * therefore, it can't be used for special swatch = can't dim swatch when pressed.
         *
         * Changed the source code here wasn't really good practice violating OCP,
         * need to fix sometime
         *
         * Added by Kei
         */
        super(context);
        mColor = color;
        mOnColorSelectedListener = listener;
        if (mColor == -1249799){
            LayoutInflater.from(context).inflate(R.layout.special_swatch, this);
            mSwatchImage = (ImageView) findViewById(R.id.special_swatch);
            mCheckmarkImage = (ImageView) findViewById(R.id.color_picker_checkmark);
        }
        else {
            LayoutInflater.from(context).inflate(R.layout.color_picker_swatch, this);
            mSwatchImage = (ImageView) findViewById(R.id.color_picker_swatch);
            mCheckmarkImage = (ImageView) findViewById(R.id.color_picker_checkmark);
            setColor(color);
        }
        setChecked(checked);
        setOnClickListener(this);
    }

    protected void setColor(int color) {
        Drawable[] colorDrawable = new Drawable[]
                {ResourcesCompat.getDrawable(getResources(), R.drawable.color_picker_swatch, null)};
        mSwatchImage.setImageDrawable(new ColorStateDrawable(colorDrawable, color));
    }

    private void setChecked(boolean checked) {
        if (checked) {
            mCheckmarkImage.setVisibility(View.VISIBLE);
        } else {
            mCheckmarkImage.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        if (mOnColorSelectedListener != null) {
            mOnColorSelectedListener.onColorSelected(mColor);
        }
    }
}
